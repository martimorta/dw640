#include "ros/ros.h"
#include "std_msgs/Int16.h"
#include "std_msgs/Float32.h"
#include "sensor_msgs/Imu.h"
#include "tf/transform_datatypes.h"

#include "../darkwater_cplus_640/darkwater/DW640.h"
#include "../darkwater_cplus_640/darkwater/MPU9250.h"
#include "../darkwater_cplus_640/darkwater/Util.h"

#include <vector>
#include <mutex>


//
// Motors Callbacks and variables ---------------------------------------------
//

std::mutex _mux_motor_speeds;
std::vector<int> _motor_speeds(6,0);

void SanitiseMotorCommand(int id, int command)
{
  if(command>255)
  {
    command=255;
    ROS_WARN_STREAM("Motor "<<id<<" Saturated High");
  }
  if(command<-255)
  {
    command=-255;
    ROS_WARN_STREAM("Motor "<<id<<" Saturated Low");
  }
  std::lock_guard<std::mutex> lock(_mux_motor_speeds);
    _motor_speeds.at(id) = command;
}

void Motor1Callback(const std_msgs::Int16::ConstPtr& msg)
{
  SanitiseMotorCommand(0, msg->data);
}
void Motor2Callback(const std_msgs::Int16::ConstPtr& msg)
{
  SanitiseMotorCommand(1, msg->data);
}
void Motor3Callback(const std_msgs::Int16::ConstPtr& msg)
{
  SanitiseMotorCommand(2, msg->data);
}
void Motor4Callback(const std_msgs::Int16::ConstPtr& msg)
{
  SanitiseMotorCommand(3, msg->data);
}
void Motor5Callback(const std_msgs::Int16::ConstPtr& msg)
{
  SanitiseMotorCommand(4, msg->data);
}
void Motor6Callback(const std_msgs::Int16::ConstPtr& msg)
{
  SanitiseMotorCommand(5, msg->data);
}

//
// SERVOS ---------------------------------------------------------------------
//

std::mutex _mux_servo_pwms;
std::vector<float> _servo_pwms(2, 1.5); // 1.5 should be middle: https://docs.darkwater.io/640.html#programming-the-640-the-c-api-servo-control
std::vector<float> _servo_mins(2, 1.0);
std::vector<float> _servo_maxs(2, 2.0);

void SanitiseServoCommand(int id, float command)
{
  if(command>_servo_maxs.at(id))
  {
    command=_servo_maxs.at(id);
    ROS_WARN_STREAM("Servo "<<id<<" Saturated High at "<<_servo_maxs.at(id));
  }
  if(command<_servo_mins.at(id))
  {
    command=_servo_mins.at(id);
    ROS_WARN_STREAM("Servo "<<id<<" Saturated Low at "<<_servo_mins.at(id));
  }

  std::lock_guard<std::mutex> lock(_mux_servo_pwms);
    _servo_pwms.at(id) = command;
}

void Servo1Callback(const std_msgs::Float32::ConstPtr& msg)
{
  SanitiseServoCommand(0, msg->data);
}
void Servo2Callback(const std_msgs::Float32::ConstPtr& msg)
{
  SanitiseServoCommand(1, msg->data);
}

//
// NODE -----------------------------------------------------------------------
//

int main(int argc, char **argv)
{
  ros::init(argc, argv, "dw640");

  ros::NodeHandle nh;
  

  // Up to 6 motors
  ros::Subscriber motor1_sub = nh.subscribe("motor1", 20, Motor1Callback);
  ros::Subscriber motor2_sub = nh.subscribe("motor2", 20, Motor2Callback);
  ros::Subscriber motor3_sub = nh.subscribe("motor3", 20, Motor3Callback);
  ros::Subscriber motor4_sub = nh.subscribe("motor4", 20, Motor4Callback);
  ros::Subscriber motor5_sub = nh.subscribe("motor5", 20, Motor5Callback);
  ros::Subscriber motor6_sub = nh.subscribe("motor6", 20, Motor6Callback);

  // Up to 2 servos
  // Get minimum and maximum values
  ros::NodeHandle nhp("~"); // private node handle to get parameters
  float p;
  if(nhp.getParam("servo1_min", p))
  {
    _servo_mins.at(0) = p;
    ROS_INFO_STREAM("Param Servo 1 min: "<<_servo_mins.at(0));
  }
  if(nhp.getParam("servo1_max", p))
  {
    _servo_maxs.at(0) = p;
    ROS_INFO_STREAM("Param Servo 1 max: "<<_servo_maxs.at(0));
  }
  if(nhp.getParam("servo2_min", p))
  {
    _servo_mins.at(1) = p;
    ROS_INFO_STREAM("Param Servo 2 min: "<<_servo_mins.at(1));
  }
  if(nhp.getParam("servo2_max", p))
  {
    _servo_maxs.at(1) = p;
    ROS_INFO_STREAM("Param Servo 2 max: "<<_servo_maxs.at(1));
  }
  ros::Subscriber servo1_sub = nh.subscribe("servo1", 20, Servo1Callback);
  ros::Subscriber servo2_sub = nh.subscribe("servo2", 20, Servo2Callback);

  // IMU
  ros::Publisher  imu_pub     = nh.advertise<sensor_msgs::Imu>("imu", 1000);
  sensor_msgs::Imu imu_msg;
  
  ros::Rate loop_rate(10);

  // Initialise Darkwater 640 motor, servo and imu

  if (check_apm())
  {
    ROS_ERROR("Error: APM Running. can´t launch IMU");
  }
  else
  {
    // Initialise Motors
    
    DW640 dw;
    dw.initialize();
    dw.setFrequency(50);
    ROS_INFO("Darkwater 640 Board Initialised successfully");


    std::vector<DW_Motor*> motors;
    motors.resize(_motor_speeds.size());
    for(unsigned int i=0;i<6;i++)
    {
      motors.at(i) = dw.getMotor(i+1);
      motors.at(i)->off();
    }

    // Initialise Servos
    std::vector<DW_Servo*> servos;
    servos.resize(_servo_pwms.size());
    for(unsigned int i=0;i<2;i++)
    {
      servos.at(i) = dw.getServo(i+1);
      servos.at(i)->off();
    }


    // Initialise IMU: Check connection and initialisation

    MPU9250 imu;
    if (imu.testConnection())
    {
      if (imu.initialize()==1)
      {
        ROS_ERROR("Error: Initialisation unsuccessful");
      }
      else
      {
        ROS_INFO("IMU Initialised successfully");

        float ax, ay, az, gx, gy, gz, mx, my, mz;

        while (ros::ok())
        {

          // Set motor velocities
          for(unsigned int i=0;i<6;i++)
          {
            std::lock_guard<std::mutex> lock(_mux_motor_speeds);
              motors.at(i)->setMotorSpeed(_motor_speeds.at(i));
          }

          // Set servos pwm
          for(unsigned int i=0;i<2;i++)
          {
            std::lock_guard<std::mutex> lock(_mux_servo_pwms);
              servos.at(i)->setPWMmS(_servo_pwms.at(i));
          }

          // Read IMU
          imu.getMotion9(&ax, &ay, &az, &gx, &gy, &gz, &mx, &my, &mz);

          // Generate Imu message
          imu_msg.header.stamp = ros::Time::now();
          imu_msg.header.frame_id = "imu";

          imu_msg.orientation = tf::createQuaternionMsgFromRollPitchYaw(mx,my,mz);
          imu_msg.angular_velocity.x = gx;
          imu_msg.angular_velocity.y = gy;
          imu_msg.angular_velocity.z = gz;
          imu_msg.linear_acceleration.x = ax;
          imu_msg.linear_acceleration.y = ay;
          imu_msg.linear_acceleration.z = az;

          imu_pub.publish(imu_msg);

          ros::spinOnce();
          loop_rate.sleep();
        }

        ROS_INFO("Switching off. Stopping motors and servos.");

        for(auto m:motors)
          m->off();

        for(auto s:servos)
          s->off();

      }
    }
    else
    {
      ROS_ERROR("Error: Connection with IMU unsuccessful");
    }
   
  }

  ROS_INFO("Good bye.");
  return 0;
}

